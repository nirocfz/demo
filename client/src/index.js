import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import Store from './Store';

ReactDOM.render(
    (
        <Provider store={Store}>
            <Router>
                <App />
            </Router>
        </Provider>
    ), document.getElementById('root'));
registerServiceWorker();
