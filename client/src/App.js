import React, { Component } from 'react';
import {Menu, Container} from 'semantic-ui-react';
import {Switch, Route, Link} from 'react-router-dom';
import Home from './Home';
import {view as Course} from './Course';

class App extends Component {
  render() {
    return (
        <div>
            <Menu>
                <Container>
                    <Menu.Item as={Link} to='/'>Cody</Menu.Item>
                </Container>
            </Menu>
            <Container style={{marginTop: '7em'}}>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route path='/course/:courseName' component={Course} />
                </Switch>
            </Container>
        </div>
    );
  }
}

export default App;
