import React, {Component} from 'react';
import {Card} from 'semantic-ui-react';
import CourseCard from './components/CourseCard';

class Home extends Component {
    render() {
        return (
            <Card.Group>
                <CourseCard name='python' />
                <CourseCard name='html' />
            </Card.Group>
        );
    }
}

export default Home;
