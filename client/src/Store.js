import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {reducer as Course} from './Course';

const reducer = combineReducers({
    course: Course,
});

const middlewares = [thunkMiddleware];

const storeEnhancers = compose(
    applyMiddleware(...middlewares),
    (f) => f,
);

export default createStore(reducer, {}, storeEnhancers);
