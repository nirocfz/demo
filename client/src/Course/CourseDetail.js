import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {List} from 'semantic-ui-react';

class CourseDetail extends Component {
    render() {
        const {courseName} = this.props.match.params;
        return (
            <div>
                this is course detail page, course: {courseName}, here are some modules in this courses
                <List>
                    <List.Item><Link to={`/course/${courseName}/module/print`}>Print</Link></List.Item>
                    <List.Item><Link to={`/course/${courseName}/module/logic`}>Logic</Link></List.Item>
                </List>
            </div>
        );
    }
}


export default withRouter(CourseDetail);
