import {FETCH_QUESTIONS_STARTED, FETCH_QUESTIONS_SUCCESS, FETCH_QUESTIONS_FAILURE} from './actionTypes';
import {combineReducers} from 'redux';

const initState = {
    loading: false,
    data: [],
};

const reducer = (state=initState, action) => {
    switch (action.type) {
        case FETCH_QUESTIONS_STARTED:
            return {
                ...state,
                loading: true,
            };
        case FETCH_QUESTIONS_SUCCESS:
            return {
                ...state,
                loading: false,
                data: action.data,
            };
        case FETCH_QUESTIONS_FAILURE:
            return {
                ...state,
                loading: false,
            };
        default:
            return state;
    }
};

export default combineReducers({
    questions: reducer,
});
