import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Switch, Route} from 'react-router-dom';
import CourseDetail from './CourseDetail';
import Question from './Question';
import {fetchQuestions} from './actions';

class Course extends Component {
    componentDidMount() {
        this.props.fetchData();
    }
    render() {
        return (
            <Switch>
                <Route path='/course/:courseName/module/:moduleName/:step' component={Question} />
                <Route path='/course/:courseName/module/:moduleName' component={Question} />
                <Route path='/course/:courseName' component={CourseDetail} />
            </Switch>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: () => {
            return dispatch(fetchQuestions());
        }
    }
};

export default connect(null, mapDispatchToProps)(Course);
