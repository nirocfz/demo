import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter, Redirect, Link} from 'react-router-dom';
import CodeBlock from '../components/CodeBlock';
import Answer from '../components/Answer';
import {Button, Segment, Message} from 'semantic-ui-react';

class Question extends Component {
    constructor() {
        super(...arguments);
        this.onAnswerChange = this.onAnswerChange.bind(this);
        this.checkAnswer = this.checkAnswer.bind(this);
        this.state = {
            answer: '',
            result: null,
            message: ''
        };
    }

    onAnswerChange(data) {
        this.setState({
            answer: data
        });
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.step !== this.props.match.params.step) {
            this.setState({
                message: '',
                result: null,
                answer: ''
            });
        }
    }

    checkAnswer() {
        // need real check here
        this.setState({
            message: 'right',
            result: true
        });
    }

    render() {
        let {courseName, moduleName, step} = this.props.match.params;
        if (step === undefined) {
            return <Redirect to={`/course/${courseName}/module/${moduleName}/0`} />;
        }
        step = Number(step);
        console.log('this.props', this.props);
        if (!this.props.current) return null;
        const question = this.props.current;
        return (
            <div>
                Course: {courseName}, Module: {moduleName}, Step: {step + 1}
                <div>
                    {question.description}
                </div>
                <div>
                    {question.text}
                </div>
                <CodeBlock
                    type={question.type}
                    content={question.question}
                    answer={this.state.answer}
                />
                <Answer
                    type={question.type}
                    onChange={this.onAnswerChange}
                    answer={this.state.answer}
                    options={[
                        {
                            label: 'hello',
                            value: 1,
                        },
                        {
                            label: 'world',
                            value: 2
                        }
                    ]}
                />
                <Button onClick={this.checkAnswer}>Check Answer</Button>
                {
                    this.state.message ?
                        <Message negative={this.state.result === false} positive={this.state.result === true}>
                            {this.state.message}
                        </Message>
                        : null
                }
                <Segment basic>
                    <Button disabled={step === 0} as={Link} to={`/course/${courseName}/module/${moduleName}/${Number(step) - 1}`}>Prev</Button>
                    <Button disabled={step === this.props.length - 1} as={Link} to={`/course/${courseName}/module/${moduleName}/${Number(step) + 1}`}>Next</Button>
                </Segment>
            </div>
        );
    }
}

const mapStateToProps = (state, props) => {
    const {step} = props.match.params;
    if (step === undefined) {
        return {};
    }
    const questions = state.course.questions.data;
    if (!questions[step]) {
        return {};
    }
    return {
        current: questions[step],
        length: questions.length
    };
};

export default connect(mapStateToProps)(withRouter(Question));
