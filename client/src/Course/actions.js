import {FETCH_QUESTIONS_STARTED, FETCH_QUESTIONS_SUCCESS, FETCH_QUESTIONS_FAILURE} from './actionTypes';
import axios from 'axios';

export const fetchQuestionsStarted = () => ({
    type: FETCH_QUESTIONS_STARTED,
});

export const fetchQuestionsSuccess = (data) => ({
    type: FETCH_QUESTIONS_SUCCESS,
    data,
});

export const fetchQuestionsFailure = (error) => ({
    type: FETCH_QUESTIONS_FAILURE,
    error
});

export const fetchQuestions = () => {
    return (dispatch) => {
        dispatch(fetchQuestionsStarted());
        return axios.get('/api/questions', {
            params: {moduleId: 1}
        }).then((res) => {
            const {code, data} = res.data;
            if (code === 0) {
                dispatch(fetchQuestionsSuccess(data));
            } else {
                dispatch(fetchQuestionsFailure());
            }
        }).catch((err) => {
            dispatch(fetchQuestionsFailure(err));
        })
    }
}
