import React from 'react';
// import {Segment, Input} from 'semantic-ui-react';
import AnswerInput from './AnswerInput';
import AnswerChosen from './AnswerChosen';

export default (props) => {
    if (props.type === 'filling') {
        return <AnswerInput {...props} />;
    } else if (props.type === 'chosen filling') {
        return <AnswerChosen {...props} />;
    } else if (props.type === 'chosen') {
        return <AnswerChosen {...props} />;
    }
    return null
};
