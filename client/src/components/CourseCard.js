import React from 'react';
import {Link} from 'react-router-dom';
import {Card, Button} from 'semantic-ui-react';

const {Content, Header, Description} = Card;
export default (props) => (
    <Card as={Link} to={`/course/${props.name}`}>
        <Content>
            <Header>{props.name}</Header>
            <Description>this is course description of {props.name}</Description>
        </Content>
        <Content extra>
            <Button size='small' color='teal'>Get Started</Button>
        </Content>
    </Card>
);
