import React, {Component} from 'react';
import {Input} from 'semantic-ui-react';

class AnswerInput extends Component {
    constructor() {
        super(...arguments);
        this.onChange = this.onChange.bind(this);
    }
    onChange(event, data) {
        this.props.onChange(data.value);
    }
    render() {
        return <Input onChange={this.onChange} />
    }
}

export default AnswerInput;
