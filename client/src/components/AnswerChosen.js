import React, {Component} from 'react';
import {Checkbox} from 'semantic-ui-react';

class AnswerChosen extends Component {
    constructor() {
        super(...arguments);
        this.onChange = this.onChange.bind(this);
    }
    onChange(event, data) {
        this.props.onChange(data.label);
    }
    render() {
        return (
            <div>
                {
                    this.props.options.map((item) => (
                        <Checkbox
                            key={item.value}
                            radio
                            label={item.label}
                            value={item.value}
                            checked={this.props.answer === item.label}
                            onChange={this.onChange}
                        />
                    ))
                }
            </div>
        )
    }
}

export default AnswerChosen;
