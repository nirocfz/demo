import React, {Component} from 'react';
import {Segment} from 'semantic-ui-react';

class CodeBlock extends Component {
    render() {
        const {type} = this.props;
        if (type === 'filling') {
            return (
                <Segment>
                    {this.props.content} ('{this.props.answer || '______'}')
                </Segment>
            );
        } else if (type === 'chosen filling') {
            return (
                <Segment>
                    {this.props.answer || '______'}
                </Segment>
            );
        } else {
            return null;
        }
    }
}

export default CodeBlock;
