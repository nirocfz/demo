import 'babel-polyfill';
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';
import config from 'config';
import ctrl from './ctrl';

const app = new Koa();

app.use(bodyParser());

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.status = err.status || 500;
    ctx.app.emit('error', err, ctx);
  }
});

const router = new Router();
router.get('/api/courses', ctrl.getCourses);
router.get('/api/modules', ctrl.getModules);
router.get('/api/questions', ctrl.getQuestions);
router.get('/api/options', ctrl.getOptions);
router.post('/api/submit', ctrl.submit);

app.use(router.routes()).use(router.allowedMethods());

const port = config.port || 3456;

app.on('error', (err, ctx) => {
  console.error('global error', ctx.method, ctx.url, err.message);
});

app.listen(port);

console.log('app is listening at port', port);
