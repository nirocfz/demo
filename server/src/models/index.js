import Sequelize from 'sequelize';
import config from 'config';

const sequelize = new Sequelize(
  config.mysql.database,
  config.mysql.user,
  config.mysql.password,
  {
    host: config.mysql.host,
    dialect: 'mysql',
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
    operatorsAliases: false,
  },
);

const db = {};
db.Courses = sequelize.import('./courses.js');
db.Modules = sequelize.import('./modules.js');
db.Questions = sequelize.import('./questions.js');
db.Options = sequelize.import('./options.js');

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
