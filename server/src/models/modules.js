export default (sequelize, DataTypes) => (
  sequelize.define('modules',
    {
      title: DataTypes.STRING(64),
      name: DataTypes.STRING(64),
      courseId: DataTypes.BIGINT.UNSIGNED,
    },
    {
      timestamps: false,
    })
);
