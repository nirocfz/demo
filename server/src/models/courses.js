export default (sequelize, DataTypes) => (
  sequelize.define('courses',
    {
      title: DataTypes.STRING(64),
      name: DataTypes.STRING(64),
      description: DataTypes.STRING(512),
      color: DataTypes.STRING(16),
    },
    {
      timestamps: false,
    })
);
