export default (sequelize, DataTypes) => (
  sequelize.define('questions',
    {
      type: DataTypes.STRING(16),
      description: DataTypes.TEXT,
      question: DataTypes.TEXT,
      answer: DataTypes.TEXT,
      moduleId: DataTypes.BIGINT.UNSIGNED,
    },
    {
      timestamps: false,
    })
);
