export default (sequelize, DataTypes) => (
  sequelize.define('options',
    {
      text: DataTypes.STRING(512),
      questionId: DataTypes.BIGINT.UNSIGNED,
    },
    {
      timestamps: false,
    })
);
