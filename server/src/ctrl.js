import models from './models';

const ctrl = {};

ctrl.getCourses = async (ctx) => {
  const data = await models.Courses.findAll();
  ctx.body = {
    code: 0,
    data,
  };
};

ctrl.getModules = async (ctx) => {
  const { courseId } = ctx.request.query;
  if (!courseId) {
    ctx.throw(400, 'missing courseId');
  }

  const data = await models.Modules.findAll({
    where: { courseId },
  });
  ctx.body = {
    code: 0,
    data,
  };
};

ctrl.getQuestions = async (ctx) => {
  const { moduleId } = ctx.request.query;
  if (!moduleId) {
    ctx.throw(400, 'missing moduleId');
  }

  const data = await models.Questions.findAll({
    attributes: {
      exclude: ['answer'],
    },
    where: { moduleId },
  });
  ctx.body = {
    code: 0,
    data,
  };
};

ctrl.getOptions = async (ctx) => {
  const { questionId } = ctx.request.query;
  if (!questionId) {
    ctx.throw(400, 'missing questionId');
  }

  const data = await models.Options.findAll({
    where: { questionId },
  });
  ctx.body = {
    code: 0,
    data,
  };
};

ctrl.submit = async (ctx) => {
  const { questionId: id, answer } = ctx.request.body;
  if (!id || !answer) {
    ctx.throw(400, 'missing questionId or answer');
  }

  const question = await models.Questions.findOne({
    where: { id },
  });
  if (!question) {
    ctx.body = {
      code: -1,
      message: `question ${id} not exists`,
    };
    return;
  }

  if (question.answer === answer) {
    ctx.body = {
      code: 0,
      data: { ret: true },
    };
  } else {
    ctx.body = {
      code: 0,
      data: { ret: false },
    };
  }
};

export default ctrl;
