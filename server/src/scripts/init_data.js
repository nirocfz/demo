import 'babel-polyfill';
import models from '../models';
import data from './data.json';

const inputArray = async (model, array) => {
  const promises = [];
  for (let i = 0; i < array.length; i += 1) {
    promises.push(model.create(array[i]));
  }
  await Promise.all(promises);
};

const input = async () => {
  console.log('data', data);
  const {
    Courses, Modules, Questions, Options,
  } = models;
  const tables = Object.keys(data);
  const promises = [];
  for (let i = 0; i < tables.length; i += 1) {
    const table = tables[i];
    switch (table) {
      case Courses.tableName:
        promises.push(inputArray(Courses, data[table]));
        break;
      case Modules.tableName:
        promises.push(inputArray(Modules, data[table]));
        break;
      case Questions.tableName:
        promises.push(inputArray(Questions, data[table]));
        break;
      case Options.tableName:
        promises.push(inputArray(Options, data[table]));
        break;
      default:
        console.warn('unknow table name');
        break;
    }
  }
  await Promise.all(promises);
};

const clean = async () => {
  const {
    Courses, Modules, Questions, Options,
  } = models;
  await Courses.destroy({ truncate: true });
  await Modules.destroy({ truncate: true });
  await Questions.destroy({ truncate: true });
  await Options.destroy({ truncate: true });
};

const main = async () => {
  await clean();
  await input();
};

main().then(() => {
  console.log('done');
});
